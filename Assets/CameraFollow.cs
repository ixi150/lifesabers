﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameBehaviour target;
    public Vector2 offset;
    public float speed = 1;

    Vector2 targetPosition
    {
        get
        {
            Vector3 delta = offset;
            if (!target.IsFacingRight) { delta.x *= -1; }
            return target.transform.position + delta;
        }
    }

    void Update()
    {
        var oldPos = transform.position;

        var newPos = Vector3.Lerp(oldPos, targetPosition, Time.deltaTime * speed);
        newPos.z = oldPos.z;

        transform.position = newPos;
    }
}
