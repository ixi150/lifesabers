﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Unit
{
    public float FallingAcceleration = 0;
    public float JumpVelocity = 1;
    public float RunVelocity = 1;
    

    Rigidbody2D rb;
    bool Grounded { get { return grounding.IsGrounded; } }
    Grounding grounding;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        grounding = GetComponentInChildren<Grounding>();
    }

    void Update()
    {
        if (Grounded && Input.GetButtonDown("Jump"))
        {
            Jump();
        }
        if (rb.velocity.y > 0 && Input.GetButtonUp("Jump"))
        {
            TryLand();
        }

        if (Input.GetButtonDown("Fire1"))
        {
            hpBarAnimator.SetTrigger("Attack");
        }
        hpBarAnimator.SetFloat("RightFaceBlend", IsFacingRight ? 1 : -1);
    }

    private void FixedUpdate()
    {
        if (!Grounded && rb.velocity.y < 0)
        {
            rb.velocity += FallingAcceleration * rb.gravityScale * Physics2D.gravity * Time.deltaTime;
        }

        var h = Input.GetAxisRaw("Horizontal");
        SetVelocityX(h * RunVelocity);
        if (h != 0)
        {
            IsFacingRight = h > 0;
        }
    }

    void TryLand()
    {
        if (rb.velocity.y > 0)
        {
            SetVelocityY(0);
        }
    }

    void Jump()
    {
        SetVelocityY(JumpVelocity);
    }

    void SetVelocityY(float y)
    {
        var v = rb.velocity;
        v.y = y;
        rb.velocity = v;
    }

    void SetVelocityX(float x)
    {
        var v = rb.velocity;
        v.x = x;
        rb.velocity = v;
    }
}
