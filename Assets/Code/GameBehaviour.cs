﻿using UnityEngine;
using System.Collections;

public class GameBehaviour : MonoBehaviour
{
    public bool IsFacingRight
    {
        get
        {
            return transform.localScale.x > 0;
        }
        set
        {
            var scale = transform.localScale;
            scale.x = Mathf.Abs(scale.x) * (value ? 1 : -1);
            transform.localScale = scale;
        }
    }
}
