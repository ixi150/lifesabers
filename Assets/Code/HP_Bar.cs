﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Serialization;
using UnityEngine;

public class HP_Bar : GameBehaviour
{
    //public GameBehaviour owner;

    public Transform hpFill, hpBack, slot;
    [Range(0, 1)]
    public float percentFilled = .8f;


    void Update()
    {
        var scale = hpFill.localScale;
        scale.x = percentFilled;
        hpFill.localScale = scale;
        scale = hpBack.localScale;
        scale.x = 1 - percentFilled;
        hpBack.localScale = scale;
    }

    private void LateUpdate()
    {
        //if (owner) { IsFacingRight = owner.IsFacingRight; }

        if (slot)
        {
            transform.position = slot.position;
            transform.rotation = slot.rotation;
        }
    }
}
