﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grounding : MonoBehaviour
{
    public LayerMask mask = -1;

    public bool IsGrounded { get; private set; }

    new Collider2D collider;
    Collider2D[] overlapResults = new Collider2D[10];

    private void Awake()
    {
        collider = GetComponent<Collider2D>();
    }

    private void FixedUpdate()
    {
        var filter = new ContactFilter2D();
        filter.SetLayerMask(mask);
        IsGrounded = collider.OverlapCollider(filter, overlapResults) > 0;
    }
}
